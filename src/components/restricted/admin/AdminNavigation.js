import React, { Component } from 'react';
import { BrowserRouter, Switch, Route  } from 'react-router-dom';
import MainRouter from '../../ComponentRouter';
import { Link } from 'react-router-dom'
import '../../../assets/css/main.css';

import Dashboard from './pages/Dashboard';
import Courses from './pages/Courses';
import Teachers from './pages/Teachers';
import CourseDetail from './pages/CourseDetail';

// import GlobalNavigation from '@atlaskit/global-navigation';

// import Avatar from '@atlaskit/avatar';


class AdminNavigation extends React.Component{

    render(){
        return (
            <div className="page padding-no-row row">
               <div className="page__menu col-sm-12 col-md-4 col-lg-2">

                   <h2 className="menu__title">Menu</h2>

                   <ul className="menu__itens">
                        <li className="itens__item"><Link to="/admin">Home</Link></li>
                        <li className="itens__item"><Link to="/admin/cursos">Cursos</Link></li>
                        <li className="itens__item"><Link to="/admin/professores">Professores</Link></li>
                        <li className="itens__item"><Link to="/admin/cursos">Alunos</Link></li>
                        <li className="itens__item"><Link to="/login">Login</Link></li>
                        <li className="itens__item"><Link to="/login">Login</Link></li>
                   </ul>
               </div> 

                <div className="page__content col-sm-12 col-md-8 col-lg-10"> 
                    <Switch>
                        <Route exact path="/admin" component={Dashboard} />
                        <Route path="/admin/cursos" component={Courses} />
                        <Route path="/admin/professores" component={Teachers} />

                        <Route path="/admin/curso/:curso_id" component={CourseDetail} />
                        <Route path="/admin/:curso_id/aula/:class_id" component={CourseDetail} />

                    </Switch>
                </div>
            </div>
            
        )
    }
}

export default AdminNavigation;

