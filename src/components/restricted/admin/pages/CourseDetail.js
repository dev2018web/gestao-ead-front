import React, { Component } from 'react';
import Auth from '../../../../services/auth';
import api from '../../../../services/api';
import { Link } from 'react-router-dom';

import AdminNavigation from '../AdminNavigation';

class CourseDetail extends Component{
  constructor(props){
    super(props);
    this.state = props;
      
    this.state = {
      course_id: this.props.match.params.curso_id,
      class_id: this.props.match.params.class_id,
      course: [],
      classes: [],
      c_class: [],
    }

  }

  componentWillMount(){
    this.getCourse();
  }

  getCourse = () => {
    api.get('/admin/cursos/'+this.state.course_id)
    .then(response => {
      this.setState({course: response.data.course}),
      this.setState({classes: response.data.course.classes})
    })
  }

  getClass = () => {
    api.get('/admin/class/'+this.props.match.params.class_id)
    .then(response => {
      this.setState({c_class: response.data.class})
    })
  }

  handleURLchange = (class_id) => {
    this.props.match.params.class_id = class_id;
    // this.setState('class_id': class_id)
    this.getClass();
  }


  render(){
    let id = 0;
    return(
      <div>
        <div className="container">
          <h1>Curso: { this.state.course.name }</h1>
          <p>{ this.state.course.description }</p>
          <div className="row classes">
            { this.state.classes.map((course_class) => 
               <ul key={course_class.id} >
                <li><Link to={'/admin/'+this.state.course_id+'/aula/'+course_class.id} onClick={() => this.handleURLchange(course_class.id)}>{course_class.name}</Link></li>
               </ul>
              )
            }
          </div>
          <hr/>

          <Aula c_class={this.state.c_class}/>
        </div>
      </div>
    )
  }
}


function Aula(props){
  const aula = props.c_class;
  console.log(aula);
  return(
    <div className="row aulas">
      <div>
      {Object.keys(aula).length > 0 ? (
        <div>
           <h1>{ aula.name }</h1>
           <div key={aula.id} dangerouslySetInnerHTML={{__html: aula.content}} />
        </div>
      ) : (
       'Nenhuma aula selecionada'
      )}
      </div>
    </div>
  )
}

export default CourseDetail;