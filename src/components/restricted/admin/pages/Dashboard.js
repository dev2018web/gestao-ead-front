import React, { Component } from 'react';
import Auth from '../../../../services/auth';
import api from '../../../../services/api';
import { Link } from 'react-router-dom';
import AdminNavigation from '../AdminNavigation';
// import Modal from '../components/Modal';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import Button, { ButtonGroup } from '@atlaskit/button';

class Dashboard extends Component{

  constructor(props){
    super(props);
    this.state = props;
    
    this.state = {
      courses: [],
      isVisible: false,
      hidden: true,
    }
    // this.closeModal = this.closeModal.bind(this);
  }

  openModalAddCurso = () => {
    this.setState({isVisible: true});
    this.setState({hidden: false});
  }

  closeModalAddCurso = () => {
    this.setState({isVisible: false});
    this.setState({hidden: false});
  }



  componentWillMount(){
    this.getDashboard();
  }

  getDashboard = () => {
    api.get('/admin/dashboard')
    .then(response => {
      console.log(response);
      this.setState({courses: response.data.courses})
    })
  }

  addCourse = (dados) => {
      api.post('/admin/course', dados)
      .then(response => {
        console.log(response);

        this.getDashboard();
        this.closeModalAddCurso();
      })
  }

  handleCourse = () => {
    let form = document.querySelector('.form-curso');
    let formData = new FormData(form);

    this.addCourse(formData);
  }

  render(){
    return(
      <div>

        <div className="container">
          <h1>Página de amnistrador</h1>
          <h2>Cursos</h2>

          <button className="btn btn-primary" onClick={this.openModalAddCurso}>Adicionar curso</button>

          <div className="row cursos">
            { this.state.courses.map((curso) => <Course key={curso.id} course={curso}/>)}
          </div>

        </div>
        
        {(this.state.hidden == false) &&
          <ModalTransition>
            {this.state.isVisible && (
              <Modal actions={[
                  { text: 'Cancelar', onClick: this.closeModalAddCurso },
                ]} onClose={this.close} heading="Adicionar curso">
                
                <form className="form-curso" enctype="multipart/form-data">
                  <div className="form-group">
                    <label for="course_name">Nome do curso</label>
                    <input type="text" className="form-control" name="name" id="course_name" placeholder="Digite o nome do curso" />
                  </div>

                  <div className="form-group">
                    <label for="description">Descrição do curso</label>
                    <textarea placeholder="Descrição do curso" class="form-control" id="description" name="description" name="description"></textarea>
                  </div>

                  <div className="form-group">
                    <label for="photo">Foto do curso</label>
                    <input type="file" class="form-control" id="photo" name="photo"/>
                  </div>  

                  <Button onClick={this.handleCourse} className="bg-success"><span className="text-white">Adicionar</span></Button>
                </form>
              </Modal>
            )}
          </ModalTransition>
        }

        

      </div>
    )
  }
}


function Thumbnail (props){
    return <img src={'http://localhost/restapi/public/storage/app/'+props.course.photo} alt={props.course.name}/>;
}

function Course(data){
  return (
    <div className="cursos__curso col col-md-12 col-md-4 col-lg-3">
      <div className="card">
        <div>
          <Thumbnail course={data.course}/>
        </div>
        <div className="card-body">
          <h5 className="card-title">{ data.course.name }</h5>
          <p className="card-text">{ data.course.description.substring(0,200) }...</p>
          <p className="card-text">{(data.course.classes.length > 0) ? data.course.classes.length + ((data.course.classes.length > 1) ? ' aulas' : ' aula') : 'Nenhuma aula nesse curso ainda'} </p>

          {(data.course.classes.length > 0) ? <Link to={'/admin/curso/'+data.course.id} className="btn btn-primary">Abrir</Link> : ''}
        </div>
      </div>
    </div>
  );
}




export default Dashboard;