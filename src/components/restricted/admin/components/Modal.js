import React, { Component } from 'react';
import animateCss from 'animate.css';
import {Animated} from "react-animated-css";



class Modal extends Component{
  constructor(props){
    super(props);

    this.state = {

    }
  }

  render(){
    let isVisible = this.props.isVisible;

    return (
      <Animated animationInDuration={300} animationOutDuration={300} animationIn="fadeIn" animationOut="fadeOut" isVisible={isVisible}>  
          <div className="modal-add-course">
              {this.props.children.map((element) =>
                <div>{ element }</div>
              )}
          </div>
          <div onClick={this.closeModal}>
          <Shaddow/>
          </div>
      </Animated>
    )
  }

}

function Shaddow(){
  return (
    <div className="shaddow">

    </div>
  )
}

export default Modal;

