import React, {component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import api from '../../services/api';
import { browserHistory } from 'react-router';

class Login extends React.Component{
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event){
        event.preventDefault();

        let form = document.querySelector(".form-login");
        
        let email = form.querySelector(".email").value;
        let password = form.querySelector(".password").value;

        this.auth(email, password);
    }

    setLogin = (token) => {
        sessionStorage.setItem('token', token);
    }

    auth = (email, password) => {
        api.get('/user?email='+email+'&password='+password, { firstName: 'Marlon', lastName: 'Bernardes' })
        .then(response => {
            this.setLogin(response.data.token);

            let prefix = '';

            switch(response.data.role){
                case 'ADMIN':
                    prefix = '/admin';
                break;
                case 'PROFESSOR':
                    prefix = '/professor';
                break;     
                case 'ALUNO':
                    prefix = '/aluno';
                break; 
            }

            this.props.history.push(prefix);
        });  
    }

    render(){
        return(
            <div className="container">
                <form className="align-middle form-login" onSubmit={this.handleSubmit}>
                    <h1>Login</h1>
                    <div className="form-group">
                        <label for="exampleInputEmail1">Endereço de email</label>
                        <input type="email" className="form-control email" id="exampleInputEmail1" name="email" placeholder="Seu email"/>
                        <small id="emailHelp" className="form-text text-muted">Nunca vamos compartilhar seu email, com ninguém.</small>
                    </div>
                    <div className="form-group">
                        <label for="exampleInputPassword1">Senha</label>
                        <input type="password" className="form-control password" id="exampleInputPassword1" name="password" placeholder="Senha"/>
                    </div>
                    <div className="form-group form-check">
                        <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                        <label className="form-check-label" for="exampleCheck1">Clique em mim</label>
                    </div>
                        <button type="submit" className="btn btn-primary">Enviar</button>
                </form>       
            </div>
        )
    }
}

export default Login;