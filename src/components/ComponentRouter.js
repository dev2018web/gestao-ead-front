import React, { Component } from 'react';
import { BrowserRouter, Switch, Route  } from 'react-router-dom';
import AdminNavigation from './restricted/admin/AdminNavigation';
import Auth from '../services/auth';
import Login from './public/Login';

class ComponentRouter extends Component{
  render(){
    return(
    <BrowserRouter>
      <Switch>
        <Route path="/admin" component={AdminNavigation} />

        <Route path="/" component={Login} />
      </Switch>
    </BrowserRouter>
    )
  }
}

export default ComponentRouter;